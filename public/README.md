# Helipad
Simple Switch-themed frontend for a DNS server.

Originally designed as a simple Nintendo Switch like interface for the SwitchBru DNS server landing page.
This is the full and complete source for Helipad. Nothing is hidden behind a paywall or proprietary licencing: it's all here.

Initially designed by pwsincd and vgmoose with controller support and a news channel added by Ep8Script, and currently maintained and worked on by supersonicsataa (FieryMewtwo)


## Accessing

Helipad can be accessed by using browseDNS and pointing the browser to it (or with browseNX if you have Atmosphere), but if you want an experience as close to SwitchBru as possible, you'll want to set up a "malicious" DNS server to redirect the connection test to Helipad.

Please enjoy.